"""
Unit tests for bookstore.py
"""
import unittest
from io import StringIO, BytesIO
from unittest.mock import patch

import bookstore as bs


class TestXMLParser(unittest.TestCase):
    """Tests for xml_parser function."""

    def setUp(self):
        self.book_xml_mock = b"""
            <bookstore>
            <book category="python">
              <title lang="en">Think Python</title>
              <author>Allen B. Downey</author>
              <year>2017</year>
              <price>29.83</price>
            </book>
            </bookstore>"""
        self.expected_output = [{'author': 'Allen B. Downey',
                                 'category': 'python',
                                 'lang': 'en',
                                 'price': '29.83',
                                 'title': 'Think Python',
                                 'year': '2017'}]

    @patch('os.path.isfile')
    def test_xml_parser_processes_file_successfully(self, mock_isfile):
        mock_isfile.return_value = True
        books = bs.xml_parser(BytesIO(self.book_xml_mock))
        self.assertEqual(books, self.expected_output)

    def test_xml_parser_raises_no_file_found_exception(self):
        self.assertRaises(FileNotFoundError, bs.xml_parser, '')


class TestPrintBooks(unittest.TestCase):
    """Tests for print_books function."""

    def setUp(self):
        self.book = bs.Book()
        self.book.title = 'Learning XML'
        self.book.year = '2003'
        self.book.category = 'programming'
        self.book.author = 'Erik Ray'
        self.book.price = '25'

        self.expected_output = "Learning XML (2003)\n" \
                               "	Category: programming\n" \
                               "	Author: Erik Ray\n" \
                               "	Price: 25\n\n"

    @patch('sys.stdout', new_callable=StringIO)
    def test_print_books_correct_output_format(self, mock_stdout):
        bs.print_books((self.book,))
        self.assertEqual(self.expected_output, mock_stdout.getvalue())


class TestGetBookData(unittest.TestCase):
    """Tests for get_book_data function."""

    def setUp(self):
        self.expected_book = {'author': '',
                              'category': '',
                              'cover': '',
                              'lang': '',
                              'price': '',
                              'title': '',
                              'year': ''}

    @patch('builtins.input', lambda x: '')
    def test_get_book_data_read_book_from_stdin(self):
        book = bs.get_book_data()
        self.assertEqual(self.expected_book, book)


if __name__ == '__main__':
    unittest.main()
