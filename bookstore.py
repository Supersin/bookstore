"""
Module to fetch books information from an xml file,
parse it, save to sqlite and output information
"""
import os.path
import sys
from argparse import ArgumentParser

from lxml import etree
from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

BASE = declarative_base()
DB_NAME = 'bookstore.db'
BOOK_DATA_FIELDS = ('author', 'cover', 'category', 'lang', 'price', 'title',
                    'year')


class Book(BASE):

    __tablename__ = "books"

    id = Column(Integer, primary_key=True)
    author = Column(String)
    title = Column(String)
    category = Column(String)
    lang = Column(String)
    cover = Column(String)
    price = Column(Integer)
    year = Column(Integer)


def xml_parser(filename: str) -> list:
    """
    Extract books data from given filename.

    Args:
        filename: path to xml file with books records.

    Returns:
        list of dictionaries with book attributes as keys and values.

    Raises:
        FileNotFoundError: If given file can't be found.
    """
    if not os.path.isfile(filename):
        raise FileNotFoundError("File '{}' not found.".format(filename))

    # create parser to recover from bad characters.
    parser = etree.XMLParser(recover=True)
    tree = etree.parse(filename, parser=parser)

    books = []

    for book_record in tree.getroot():
        book = {}
        authors = []
        book.update(book_record.attrib)

        for subrecord in book_record:
            if subrecord.tag == 'author':
                authors.append(subrecord.text)

            book[subrecord.tag] = subrecord.text
            book.update(subrecord.attrib)

        books.append(book)
        book['author'] = ', '.join(authors)

    return books


def create_db(base: declarative_base, database_name: str):
    """
    Create database if not exist.

    Args:
        base: sqlalchemy declarative_base class.
        database_name: path to xml file with books records.

    Returns:
        sqlalchemy engine instance.
    """
    db_path = os.path.join(os.path.dirname(__file__), database_name)
    engine = create_engine('sqlite:///{}'.format(db_path))
    base.metadata.create_all(engine)
    return engine


def get_book_data() -> dict:
    """Collect book data from command line input.

    Returns:
        dictionary with book parameters as key-values pairs
    """
    book = {}
    for i in BOOK_DATA_FIELDS:
        book[i] = input('Enter a book {}: '.format(i))

    return book


class BookManager(object):
    """"""

    def __init__(self, db_engine):
        self.db = db_engine
        db_session = sessionmaker(bind=db_engine)
        self.session = db_session()

    def add_one(self, book: dict) -> Book:
        """
        Add single book.

        Args:
            book: dictionary with book parameters.
                  See BOOK_DATA_FIELDS for allowed fields.

        Returns:
            Book object
        """
        new_book = Book(**book)
        self.session.add(new_book)
        return new_book

    def remove(self, book_title: str):
        """
        Remove book from database by title.

        Args:
            book_title: title of book to remove.
        """
        book = self.get_one(book_title)
        self.session.delete(book)
        self.session.commit()

    def populate(self, books: list):
        """
        Add list of books records to database.

        Args:
            books: list of dictionaries with book parameters.
                   See BOOK_DATA_FIELDS for allowed fields.
        """
        for book in books:
            self.add_one(book)
        self.session.commit()

    def get_one(self, book_title: str) -> Book:
        """
        Get one book from database by title.

        Args:
            book_title: title of book to remove.

        Returns:
            Book object
        """
        book = self.session.query(Book).filter_by(
            title=book_title).one_or_none()
        if book is None:
            raise NameError("Book '{}' not found.".format(book_title))
        return book

    def get_books(self) -> list:
        """
        Get all books from database.
        """
        return self.session.query(Book).all()


def print_books(books: iter):
    """
    Pretty print books.

    Args:
        books: list of Book objects.
    """
    for book in books:
        book_year = ('({})'.format(book.year) if book.year else '')

        title = ' '.join((book.title, book_year))
        fields = '\n\tCategory: {}\n\tAuthor: {}\n\tPrice: {}\n'.format(
            book.category, book.author, book.price)

        lang = ('\tLanguage: {}\n'.format(book.lang) if book.lang else '')
        cover = ('\tCover: {}\n'.format(book.cover) if book.cover else '')

        print(''.join((title, fields, lang, cover)))


def main():
    parser = ArgumentParser()
    parser.add_argument('-f', '--file', dest='filename', type=str,
                        help='Get books from given XML file, write to DB '
                             'and print to output.')
    parser.add_argument('-rm', dest='book_name', type=str,
                        help='Remove book by name.')
    parser.add_argument('-add', action='store_true',
                        help='Add book from command line.')

    args = parser.parse_args()

    engine = create_db(BASE, DB_NAME)
    manager = BookManager(engine)

    if args.filename:
        books = xml_parser(args.filename)
        manager.populate(books)
        print_books(manager.get_books())

    elif args.add:
        book = get_book_data()
        new_book = manager.add_one(book)
        print_books((new_book,))

    elif args.book_name:
        manager.remove(args.book_name)
        print('{} successfully deleted.'.format(args.book_name))


if __name__ == '__main__':
    try:
        main()
    except (FileNotFoundError, NameError, SQLAlchemyError) as err:
        sys.exit(err)
